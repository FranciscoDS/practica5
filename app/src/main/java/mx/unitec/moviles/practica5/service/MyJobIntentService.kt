package mx.unitec.moviles.practica5.service

import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.core.app.JobIntentService

class MyJobIntentService: JobIntentService() {
    val Tag = "MyFirstJob"
    override fun onHandleWork(intent: Intent) {
        val max = intent.getIntExtra("max", -1)
        for(i in 0 until max){
            Log.d(Tag, "working: Numero $i")
            try{

            }catch (e:InterruptedException){
                e.printStackTrace()
            }
        }
    }
    companion object{
        private const val JOB_ID = 2

        fun enqueueWork(context: Context, intent: Intent ){

            enqueueWork(context, MyJobIntentService::class.java, JOB_ID, intent)
        }

    }

}